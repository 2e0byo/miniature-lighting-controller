% Created 2021-01-06 Wed 16:38
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{dsfont}
\author{John Morris}
\date{\today}
\title{Miniature Lighting Controller Documentation}
\hypersetup{
 pdfauthor={John Morris},
 pdftitle={Miniature Lighting Controller Documentation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.4.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{How to read this file}
\label{sec:org06f09d1}

This file is in several  formats: it’s supplied as a printed copy,
because printed documentation gives an aurora of respectability, as
Mrs Malaprop would say.  It’s also on the supplied USB stick, as a pdf
document and as an html document.  The pdf, like the html, is
searchable and crosslinked.  It’s also in colour.  Lastly, it’s
online, together with all the code, at
\url{https://gitlab.com/2e0byo/miniature-lighting-controller}.  Oh, and it’s
also in OrgMode, a text-based markup language used to write it, but
you probably don’t care about that.

If you just want to \emph{use} the thing, the paper should get you
started---just keep reading.  If you want to \emph{undertand} something,
you’re probably better off at the computer.

To \emph{use} the Lighting Controller you just need to know how to turn it
on and load up the controlling software.  The rest of this manual
describes other things which are possible and might (who knows?) be fun.

\section{Uses}
\label{sec:org79cd60c}

This is a lighting controller, exactly like lighting controllers in a
theatre.  It has eight channels, numbered 0-7 (since I am a lazy
programmer, and computers start counting at 0).  These can be set
between off (0) and full on (256---no, that’s not a typo, see
\ref{sec:orge092583} to find out why).  At 0 the corresponding pair of
output terminals has no voltage accross it.  At 256 it has the voltage
displayed on the little display accross it.  This voltage can be
adjusted by turning the knob, but it applies to all channels (so you
can only really control lamps or outputs which want the same voltage
accross them, although see \ref{sec:orgd9d443a} below.)

You can drive standard filament lamps---the kind you’ve used
before---directly.  You can also drive some LEDs directly, and others
with an appropriate current limiting resistor (see \ref{sec:orgbc065b2} below).  You
could also theoretically drive any other resistive sink, or indeed an
inductive sink like a motor, as protection diodes are fitted.

The controller (the hardware) is controlled over USB, by a computer
running software.  It ships with two demo programs: a graphical
program consisting of eight sliders which can be used to control the
channels, exactly like a ‘real’ theatre controller, together with the
ability to load and save states, for use as presets, and a
demonstration of scripting the controller, which is discussed in the
section \ref{sec:orgf68b358}.

The controller is powered by an external SMPSU, which sometimes gets a
little warm in operation, but which is capable of running for extended
periods wtihout overheating.  The SMPSU can supply up to 30v, but is
set to supply 15v at the moment, which is probably all you want to
handle.  Each channel can provide around 300mA, which should be enough
for all realistic uses.

\subsection{Concept}
\label{sec:org179950c}

I imagined this functioning rather like a standard lighting
controller, only for stopmotion sets, controlling one or two lights,
as you did with batteries in Fydor.  However it could handle more: for
instance, a set of chritmas lights could be fashioned into
streetlamps, and turn on fitfully, like real lamps lighting up, by
scripting a ‘turn on’ program.  If you weren’t shooting in realtime
but could control the camera from the computer, each step in the
fitful turn-on could happen a few milliseconds before the shot is
taken, saving you from setting the manually (although I imagine this
kind of thing would probably be filmed in realtime).

Likewise, it could be used just as something to play with: for several
days it faded a string of lights on and off in a sinusoidal pattern in
the living room.  If you want, it would be possible to add the ability
to store patterns (of a limitied duration).

It could also be used for more exotic setups: one could attach a relay
controlling a mains light to a channel, although one could only turn
it on and off (and care must be taken when dealing with mains
electricity!).  It could drive some kinds of motors with speed
control.  It could drive small heating elements, though what you’d
want small heating elements for I don’t know.

Lastly, if you get bored, learning to program it is a fair
introduction to coding in Python, an easy and very versatile language
which is used everywhere.

\section{How to use}
\label{sec:org6a9e9bd}

Connect to Computer.  Plug in power supply.  Set correct voltage.
Attach lamps etc. to each channel.  Load software.

If you have any trouble connecting to the controller (after installing
the software), turn it off and on again whilst still plugged in to the
computer.

\subsection{Hardware}
\label{sec:orgbca25ad}

Connections are by screw-terminal (chocolate block).

\subsubsection{LEDs}
\label{sec:orgbc065b2}

LEDs can be driven, but need more care than filament lamps.  In
general LEDs should be driven in constant \emph{current} mode, not constant
\emph{voltage}.  This is because, although they have a constant \emph{threshold}
voltage (the voltage, or rather narrow range of voltages, at which
they start glowing), they represent a very low-resistance load above
this voltage (effectively a short circuit).  Thus as the voltage
climbs they absorb a \emph{lot} of power and catch fire.  This is inverse
to the behaviour of a filament lamp, which is effectively a short
circuit when connected, but rapidly gains in resistance as it heats
up, and so consumes less and less power until it reaches a stable
state.

If driven with a constant \emph{current} of the right amount, LEDs will
operate at the correct voltage.  Thus, when a given current is put
through them, they have a given, fixed voltage accross them.  We can
take advantage of this to drive them with a series resistor.
According to Ohm’s Law, \(V=IR\), so \(I=V/R\).  V is fixed, but we can
change R. For an LED with a working voltage of 2.5v at 10mA (or
0.01A), with the controller set to 6V, the voltage accross the
resistor will be \(6-2.5=3.5\), and the resistor needs to be \(R=V/I =
3.5/0.01 = 350 \Omega\).  They don’t make 350 ohm resistors, so pick the
next largest: 390.  This value is rather high, but I made these
numbers up.

What about the christmas lights?  They have no resistor at all!
Another set I bought \emph{did} use a series resistor, but in this case the
manufacturer has paralleled all the LEDs, and matched them so well
that they share current equally.  (It’s quite easy when you make them
in the hundreds of thousands).  Thus they represent one big LED.  This
big LED sinks a lot more current than a single LED---most only sink a
few mA and can be blown up by a few tens of mA.  These all sink about
30mA.  Moreover, they have a working voltage of 3v.  If we go slightly
above the working voltage with a standard LED we supply a \emph{lot} more
current than it can take.  But if we do so with these, we have more
room for error.  Also, we actually \emph{do} have a series resistor---the
controller’s internal resistance!  I estimate this to be in the order
of several ohms, perhaps even 10 ohms.  It is largely caused by the
switiching MOSFETs on-resistance (see \ref{sec:orgbca25ad}).

\subsection{TL;DR}
\label{sec:orgea96c16}

If this sounds like a lot maths: most leds can be driven with a 150\(\Omega\)
resistor from 5v without any problems.  Note that they only conduct
one way.  If you want to drive from a higher voltage, scale the
resistor appropriately.  The supplied red LED is a ‘high brightness’
kind (i.e. it turns on early) and is in serials with a 1k (1,000\(\Omega\))
resistor, which makes it safe at least up to 15v, but it still turns
on appreciably at 3!  I wanted it to be hard for you to blow it up.

Christmas lights can generally be driven as in the original.  If there
is no series resistor, you don’t need one either.  If there is a
series resistor, you can simply cut it out and use it, setting the
controller to the same voltage.

\subsubsection{Differing Voltages}
\label{sec:orgd9d443a}

What do you do if you want to control two lamps of different voltage
ratings at the same time?  You \emph{might} think that you could use e.g. a
6v and a 12v lamp at the same time, by never taking the 6v channel
above 50\%.  And so you can---with a caveat: the controller uses PWM
(Pulse Width Modulation) to adjust the effective output voltages,
i.e. it switches on and off very fast.  So the lamps need to be able
to survive a pulse at the maximum brightness.  Filament lamps are fine
about this, and LEDs often are.  Note that in this case the resolution
of the lower lamp is reduced---i.e. if you have a 6V light never
driven above 50\%, there are only 128 steps, not 256.

\subsubsection{Limitations}
\label{sec:org0766c05}

The hardware has a latency: it cannot switch instantaneously.  This is
likely not a problem, but could be if you try very high-frame rate
filming.  It is possible the switching frequency could create aliasing
effects with the shutter speed, although I highly doubt it.  In this
case contact about changing it.

\subsection{Software}
\label{sec:org956abab}

\subsubsection{Installation}
\label{sec:org79b1e0f}

Since you are on a Mac (hurrah!  beats windows!  Almost an Operating
System!) we use a bash script (named .command because mac likes to do
stuff like that).  So just double-click \texttt{install.command}.  This will:

\begin{itemize}
\item Install the Homebrew package manager (which is very small, and makes
installing software as easy on Macs as on Linux)
\item Install the Python programming lanaguage (which is only a few
hundred Mb)
\item Install Libusb so we can access the usb ports easily (this is the
essential step, yet the library is tiny)
\item Install a few python modules to run the software.
\end{itemize}

Once the installation is finished (you only need to do it once) you
can just double-click to run either ‘gui.py’ or ‘demo.py’, or
right-click and open in IDLE, then press ‘run’.

Do run the demo: it’s quite fun.

\subsubsection{Graphical}
\label{sec:org87adec7}

The GUI is hopefully self explanatory.

\subsubsection{Why Python?}
\label{sec:org5fc20c1}

Why didn’t I build a ‘proper’ stand alone application?  For starters
it would be hard, and pyusb is very easy.  Secondly, I don’t have a
Mac to develop on, and know nothing about developing on Macs (though I
have tested on Emma’s).  Thirdly, and most importantly, installing
python and pyusb means \emph{you can code the thing yourself}.  Here is
what \texttt{sloccount}, which counts Single Lines of Code, thinks of the
gui:

\begin{verbatim}
Total Physical Source Lines of Code (SLOC)                = 62
Development Effort Estimate, Person-Years (Person-Months) = 0.01 (0.13)
 (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))
Schedule Estimate, Years (Months)                         = 0.10 (1.15)
 (Basic COCOMO model, Months = 2.5 * (person-months**0.38))
Estimated Average Number of Developers (Effort/Schedule)  = 0.11
Total Estimated Cost to Develop                           = $ 1,458
 (average salary = $56,286/year, overhead = 2.40).
SLOCCount, Copyright (C) 2001-2004 David A. Wheeler
\end{verbatim}

Well, there you are, you have \$ 1,458 of code.  Which is utterly
ridiculous.  But then I’m not paid \$ 56,286/year: far from it.  More
to the point you have a \emph{mere 62 lines} of code for the whole thing
(excluding comments, etc).  What about the demo?  \texttt{sloccount} reports
31 lines of code.  Coding in python is \emph{easy}.  (Particularly as I’ve
done all the hard work for you and written an asynchronous Hardware
Abstraction Layer, which clocks in at 133 sloc.)  To get an idea just
how easy this is, take a look at the \ref{sec:orgb7dd3cf} below.

\section{Implementation}
\label{sec:orge092583}

A circuit diagram is provided, since it’s a good idea to have such
things.  Note that the voltage regulator is not shown: this is a
module, bought from Amazon or Ebay ages ago, and is switch-mode (so it
shouldn’t get too hot).  I have no idea of its specifications, but the
worst case scenario---300mA on each channel, or \(8\times300 = 2.4A\) is
probably pushing it.

\subsection{Hardware}
\label{sec:orgb77c7fa}

The core of the hardware is a PIC18f4550 microcontroller, a small
one-chip computer running at 12MHz (compare that to your laptop,
running probably at several GHz).  It has integrated USB hardware, and
is running the \href{https://pinguino.cc/}{Pinguino} bootloader, which allows updating the firmware
over usb.

\subsubsection{Firmware}
\label{sec:org9252ede}

The code is somewhat tricky to read, as it is hacked
together from the Pinguino original (Pinguino uses simple ‘sketches’
to generate a large number of files which are then compiled to produce
the machine-code running on the microcontroller), but the core routine
is simple enough:

\begin{minted}[]{c}
receivedbyte = BULK_read(buffer); /* returns index of end */

unsigned char channel;
unsigned long duty;
inputPTR = buffer;

if (!strncmp(inputPTR, "s", 1)) {

  channel = ascii_to_decimal(inputPTR[1]);

  duty = 100* ascii_to_decimal(inputPTR[2]) + 
	 10*ascii_to_decimal(inputPTR[3]) + 
	 ascii_to_decimal(inputPTR[4]);

  if (duty>256) duty = 256;
  newDuties[channel] = duty;
  BULK_printf("Set channel %u to %u", channel, duty);
}
else if (!strncmp(inputPTR, "g", 1)) {

  BULK_printf("Channel %u is %u", channel, newDuties[channel]);
}
\end{minted}

This is in C, which is harder than python, at any rate to read as a
non-coder.  I’ve also cut all the error detection and a few other
things: see \texttt{pic/user.c} if you want to see the whole thing.

This code, which runs in a loop which otherwise sits around waiting
for data, looks to see if the input (obtained by calling \texttt{BULK\_read})
starts with ‘s’, and if so, Sets the given channel to the value given
(after converting it from ascii to decimal, i.e. from a string like
\texttt{"4"} to the number \texttt{4}), makes sure it’s no bigger than 256, and then
writes the new value to an array, \texttt{newDuties}.  This array is checked
by the interrupt routine which happens very quickly, thousands of
times a second, and which, in simplified form, looks like this:

\begin{minted}[]{c}
++count;
unsigned char latch = LATD;
if (count == 0) {
  latch = 0;
  for (unsigned char i=0; i<8; i++) {
    duties[i] = newDuties[i];
  }
}
for (unsigned char i=0; i<8; i++) {
  if (duties[i] == count) latch |= (0x1 << i);
}
LATD = latch;
\end{minted}
This, then, c
Firstly we update a variable ‘count’.  This variable is defined
elsewhere as being only 8 bits long, i.e. the maximum value it can
store is \(2^8 -1 = 255\).  If we try to add one to it when it stores
255, it will ‘roll over’ to 0 again, which is handy.  So we have 255
steps.

Then we set a temporary variable to the value of \texttt{LATD}, which is the
value of the 8-bit output port controlling the channels, or, in simple
terms, the value of the channels.  So if LATD is ‘11110000’, channels
0-3 will be off, and 4-7 will be on (note: numbers are right-to-left
when written down!).  We store this in a variable, since writing to
LATD \emph{changes the actual outputs}, which is pretty nifty.  

Then, only if count is ‘0’, we set the temporary ‘latch’ variable to 0
(i.e. all channels off), and set the ‘duties’ array to be the same as
the ‘newduties’ we modified earlier.  We don’t modify ‘duties’
directly to avoid flickering, which would happen if we updated
‘duties’ halfway through a cycle.

Then we go through each channel in turn, and if the value of \texttt{count}
is the same as the value of \texttt{duties[i]} (that is: the \texttt{ith} value in
the array \texttt{duties}) turn the \texttt{ith} channel on.  (This is what the
weird thing with the \texttt{<<} does: it sets the \texttt{ith} bit of our ‘latch’
variable.)  Note that we start by turning outputs \emph{off}, and turn them
\emph{on} when we get to the value transmitted.  So to turn a channel \emph{off}
you send 256, which is 1 more than 255 and so will \emph{never} cause it to
be turned on.  This is the reason for the weird resolution---255 steps
\begin{itemize}
\item 1 to turn it fully off = 256 steps.  The HAL turns this round for
\end{itemize}
you, so \texttt{set\_brightness(256)} is full on and \texttt{set\_brightness(0)} is
off, as you would expect.  This is another reason to use a HAL.

Then, finally, we set the real output to be whatever we have assembled
in our temporary \texttt{latch} variable.



\subsubsection{Circuit}
\label{sec:orgf0da279}

The circuit is simplicity itself.  The outputs from the PIC are either
at 0v or at 5v.  They are connected to 2n7000 mosfets, which are in
this usage like switches, which turn on if 5v is applied to the gate
and off if 0v is applied.  They don’t like handling more than a few
hundred mA, though, so bigger mosfet switches might be needed if you
wanted to switch more.

\subsection{Software}
\label{sec:org0741a0f}

There are two provided software interfaces.  One is the demo
(\texttt{demo.py}) which shows how easy it is to code arbitrary actions.  The
other is a very simple gui, which has 8 sliders like a real hardware
lighting controller, and the ability to load or save states.  These
saved states are just single-line csv files: consult the code to find
out more.

\section{Coding}
\label{sec:orgf68b358}

\subsection{Quick Start Guide}
\label{sec:orgb7dd3cf}

The easiest way to start off is to open and edit the demo I have
provided.  So open \texttt{demo.py} in IDLE (Python’s inbuilt editor) and
let’s have a look.  It begins like this:

\begin{minted}[]{python}
from math import pi, sin
from time import sleep

import async_hal as hal  # hal = Hardware Abstraction Layer

lighting_controller = hal.Controller()
red_lamp = hal.Channel(lighting_controller, 7)
white_lamp = hal.Channel(lighting_controller, 0)

# set brightness in background
red_lamp.set_brightness(0)
\end{minted}

Copy this much into a new file called \texttt{quick\_start.py} and save it 
\emph{in the same place you found demo.py}---probably just on the usb
stick.  Then change the last line to read:

\begin{minted}[]{python}
red_lamp.set_brightness(128)
\end{minted}

and press ‘run’.  There.  You wrote your first controlling script.

The window which popped up when you pressed ‘run’ should still be
open.  Type into it:

\begin{minted}[]{python}
red_lamp.set_percent_brightness(0.75)
\end{minted}

and observe what happens.  Then press Ctrl-P: the line you just typed
should appear.  Change the 0.75 to 0.25.  Press enter, and observe
what happens.  Press Ctrl-P, then press it again: do you see how it
lets you go ‘back’ through your input?  Replacing the ‘p’ with an ‘n’
lets you go ‘forward’.  In this way you can do less typing.

Try:

\begin{minted}[]{python}
white_lamp.fade_on()
\end{minted}

Notice how the white lamps takes around a second to fade on, but the
code returns immediately: it’s fading in the background
(‘asynchronously’).  This may or may not be what you want, depending
on what you’re trying to do.

\subsection{Less Quick Guide}
\label{sec:org5377f6a}
There, that was easy!   Let’s take another look at demo.py.  Here is
the beginning again:

\begin{minted}[]{python}
from math import pi, sin
from time import sleep

import async_hal as hal  # hal = Hardware Abstraction Layer

lighting_controller = hal.Controller()
red_lamp = hal.Channel(lighting_controller, 7)
white_lamp = hal.Channel(lighting_controller, 0)

# set brightness in background
red_lamp.set_brightness(0)
\end{minted}

Anything after a \texttt{\#} is a ‘comment’, i.e. it’s for you, not the
computer, which just ignores it.  The first two lines ‘import’ things
from ‘libraries’---i.e. ‘other code’.  The ‘math’ library is built
into Python, and provides mathematical functions and constants
(obviously).  The time library, amazingly enough, provides things to
do with time, and is likewise built in.

\texttt{async\_hal} is a library I wrote, distributed with the demo.  It’s
called \emph{async} hal as I first wrote a \emph{synchronous} hal, but then
abandoned it as the async one is much better.  (It send the usb
requests in the background.) A hal is a Hardware Abstraction Layer,
i.e. something which does the hard work of talking to the hardware.

This hal provides a \emph{class}, which is a way of assigning
representations to objects---in this case the physical object in front
of you.  From your point of view it’s just a function which returns an
‘object’, which we call \texttt{lighting\_controller}.

Once we have an object representing the controller, we make an object
for each of the channels:

\begin{minted}[]{python}
red_lamp = hal.Channel(lighting_controller, 7)
\end{minted}

The \texttt{Channel} class in the hall represents a channel \emph{on a
controller}.  So you have to tell it \emph{which} controller.  We only have
one: the one we just made a representation of!  We also need to know
which channel on the ‘hardware’ correlates with this channel on the
‘software’: here’s it’s number 7.

This is conceptually quite complicated, but very easy in practice.
For instance, let’s make a Channel object for the third channel.
Remember we start counting at 0:

\begin{minted}[]{python}
third_channel = hal.Channel(lighting_controller, 2)
\end{minted}

We could have called it something else.  Perhaps we have a street lamp
attached to channel 3:

\begin{minted}[]{python}
street_lamp = hal.Channel(lighting_controller, 3)
\end{minted}

The \emph{name} we give it is just a variable.  It can contain pretty much
anything, if we stick to the alphabet and the underscore (\texttt{\_}).

Each channel object has few methods.  Here they are:

\begin{minted}[]{python}
white_lamp.set_brightness(128)
white_lamp.set_percent_brightness(0.1)
white_lamp.get_brightness()
white_lamp.get_percent_brightness()
white_lamp.fade_on(1)
# wait a second before typing
white_lamp.fade_off(100)
white_lamp.cancel_fade()
\end{minted}

Additionally, when we initialise a Channel object it queries the
controller for the current setting on that channel, so the
software never gets out of sync with the hardware.  (This is used in
the gui to make the sliders all appear at the right place.)

\subsubsection{Going further}
\label{sec:orgb5d88a2}
Read the rest of \texttt{demo.py}.  Can you see how it works?

\subsection{Implementation documentation}
\label{sec:orgf5e2606}

It’s bad practice to ship code without documentation aimed at other
coders.  Thus this section is technical.

\subsubsection{Overview HAL}
\label{sec:org26c5f88}

The controller interacted with using a \texttt{Controller()} object provided by
the hal.  Only the asyncio hal is developed.  The expectation is that
code will not be primarily asyncio-based, so the Controller() object
starts its own asyncio thread and manages it.  Communication is by
Bulk transfers, and is not awaitable (since AFAIK pyusb isn’t
awaitable yet).  In any case it’s so fast we don’t need to worry.  In
point of fact we merely await delays in various fade steps.

On initialisation the Controller object starts an asyncio thread,
grabs the usb device (specifically: the first it finds, so if we need
to control multiple devices we’ll have to edit it to accept a bus/dev
manually) and then waits.  It provides a \texttt{submit\_async} method which
must be used to run awaitables, returning a futures.

The \texttt{Channel()} object represents a distinct channel on the
controller.  It takes a \texttt{Controller} object as an argument.  On
initialisation it queries the hardware for the current channel value.
It then keeps track of this locally, except when an asynchronous fade
is cancelled, when it queries again to discover what has actually
happend.  It provides methods to get and set brightness, either as a
raw number or a percentage (float between 0.0 and 1.0), and
asynchronous \texttt{fade\_on} and \texttt{fade\_off} methods, as well as
\texttt{cancel\_fade} to cancel a running fade.  Note that precision in timing
is not promised with these methods, and userspace fading should be
done where precision matters.  Note also that the \texttt{Channel()} class is
wholly synchronous: it merely submits tasks to the \texttt{Controller()}
which in turn submits them to its \texttt{asyncio} thread.  If multiple
controllers are desired, the code should probably be extended to
enable sharing this asyncio event loop.

\subsubsection{Overview GUI}
\label{sec:orgd1b4459}

I am not a gui programmer.  This is a very simple gui in tkinter.  It
uses ttk (Themed Tkinter) for slightly better-looking graphics.

A \texttt{ChannelSlider} class both creates the \texttt{tkinter.ttk.Scale} object
which draws the slider and the \texttt{async\_hal.Channel} object which
controls it.  Then we have a getter and setter.  The getter
just returns the slider value; the setter sets both the \texttt{Channel} by
calling \texttt{set\_brightness} and the slider.  We cast to \texttt{float} and then
\texttt{int} since ttk returns float strings but tk int strings (at least in
my testing), and to show how it’s done.

State is handled with single line csvs consisting of nothing more than
than the channel values as ints.

\subsubsection{Further details}
\label{sec:orgd866801}
See the code, it’s very simple.

\section{Extending}
\label{sec:org95d4ce2}

Do you want the hardware or software to do other things?  

\subsection{Hardware}
\label{sec:org873172f}

\subsubsection{Mains Dimming}
\label{sec:orgb87400d}

It would be possible to add a trailing-edge mosfet dimmer to functions
like a standard dimmer switch.  Contact me if you want to do this.

\subsubsection{Multiple Voltages}
\label{sec:org386d0d9}

It would be possible to add another voltage regulator inside to drive
some channels, so that they could be controlled in, say, two groups.
But if you need to do this a lot (and can’t use the trick above), it
would be better to build another controller, which would probably be
about as much work.

\subsubsection{Parallel outputs?}
\label{sec:org15f2ac9}

What if you want more current? You \emph{can} theoretically run two outputs
in parallel, as they switch at the same time, but you should be
careful to make sure they really \emph{do} have the same value all the
time.  This would be quite easy if we were scripting, we could do
something like:

\begin{minted}[]{python}
def set_parallel_output(brightness):
    output_one.set_brightness(brightness)
    output_two.set_brightness(brightness)

set_parallel_output(128)
\end{minted}

where \texttt{output\_one} and \texttt{output\_two} are already defined, like
\texttt{red\_lamp} etc in the scripting section.  Alternatively you could have
one slider control multiple outputs.  Have a go at implementing this
if you want it, but ask me if you need help.

Paralleling more than two outputs is probably a bad idea.  What are
you tring to drive which needs so much current?  It would be possible
(and easier) just to modify the hardware to use a beefier MOSFET.
They are very cheap.

\subsubsection{A nicer case!}
\label{sec:orgb784df0}

Yes, the case is really not of the best.  It was made in the freezing
cold in a great hurry.  Since you now have a 3D printer, perhaps you
would like to make a better one?

\subsection{Sofware}
\label{sec:org8e2b69f}

\subsubsection{Example: a ‘zero’ button}
\label{sec:org2d2dbd2}

Take a look at \texttt{gui.py}.  First open it and then save it under a new
name, so we can go back and retrieve the original if need be.  Call
the new name \texttt{gui2.py}.

We are going to add a ‘zero’ button which zeros every channel.  For
this we need a function which zeros channels.  After the lines

\begin{minted}[]{python}
for i in range(8):
    channels.append(ChannelSlider(i, master))
\end{minted}

add the following:

\begin{minted}[]{python}
def zero():
    """Zero everything."""
    for channel in channels:
	channel.set(0)
\end{minted}

Now we need a button to call this function.  After:

\begin{minted}[]{python}
save_button = Button(master, text="Save State", command=save_state)
save_button.grid(column=8 - 3, row=3)
\end{minted}

add:

\begin{minted}[]{python}
zero_button = Button(master, text="Zero", command=zero)
zero_button.grid(column=0, row=3)
\end{minted}

save it and run.  Now you have a zero button.

\begin{enumerate}
\item Excercise: a ‘max’ button
\label{sec:orge478b63}

Can you implemenet another button, to set every channel to the
maximum?  Hint: look at the specifications above (or the source code
for the HAL) to find out what the maximum \emph{is}, numerically.

\item Harder Excercise: a master slider
\label{sec:orgc129664}

What if you wanted a 'master' slider, which let you set the brightness
of \emph{all} the channels?

Assume that you want the slider to function as a multiplier (i.e. it
doesn't actually move the other channels, but they get multiplier by
it before the controller is set.)

This will require editing the definition of \texttt{ChannelSlider}.  I
personally would just edit the \texttt{set} method.  Perhaps something like:

\begin{minted}[]{python}
def set(self, val):
    self.channel.set_brightness(int(float(val) * master_slider.get() / 100))
    self.slider.set(int(float(val)))
\end{minted}

and then define \texttt{master slider} as something like:

\begin{minted}[]{python}
master_slider = Scale(master, from_=100, to=0, length=300, orient="horizontal")
\end{minted}

and place it below the other sliders.

Get in touch if you try this and have problems: I've only provided the
outline here.
\end{enumerate}
\end{document}