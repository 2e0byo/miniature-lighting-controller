from math import pi, sin
from time import sleep

import async_hal as hal  # hal = Hardware Abstraction Layer

lighting_controller = hal.Controller()
red_lamp = hal.Channel(lighting_controller, 7)
white_lamp = hal.Channel(lighting_controller, 0)

# set brightness in background
red_lamp.set_brightness(0)
# fade on in background
red_lamp.fade_on()
print("Sleeping for 2 seconds whilst we fade on in the background")

for i in range(1, 11):  # range(start, end): start at 1 and end at 10 (1 before 11)
    print(f"Have slept {2*i/10}s...", end="\r")
    sleep(2 / 10)

print(
    "Note how we didn't create a new line.  Have a look at the code: can you see why?"
)

for brightness in range(101):
    print(f"Setting red lamp brightness to {brightness}%")
    #  this is an 'f' string to insert the value of the variable 'brightness'

    red_lamp.set_percent_brightness(brightness / 100)  # note percent = 0-1

    sleep(0.1)  # sleep 0.1s

print("Turning white lamp on")
white_lamp.set_percent_brightness(1)

print("Sleeping for 1 second")
sleep(1)
print("Turning white lamp off")
white_lamp.set_percent_brightness(0)

print("Now we define a function and use it to flash the white light ten times.")


def flash(channel):
    """This is a help string: it tells us what the function does
    (here: flash a channel).  It is optional but a good idea.

    In this function we use the sin() function to set the brightness
    follow a sine-wave, rather than just turning on or off.  This is
    to demonstrate the ease with which we can make the output follow
    an arbitrary function.

    We import the sin() function and the number pi from the math
    library earlier: have a look at the top of the file.
    """
    for step in range(100):  # we have 100 steps
        channel.set_percent_brightness(sin(pi * step / 100))
        sleep(0.01)


for i in range(10):
    flash(white_lamp)
