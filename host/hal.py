import usb

# usb constants
VENDOR = 0x04D8
PRODUCT = 0xFEAA

ENDPOINT_OUT = 0x01

# if bootloader v4.x
CONFIGURATION = 0x01
ENDPOINT_IN = 0x81

# if bootloader v2.x
# CONFIGURATION = 0x03
# ENDPOINT_IN = 0x82

class ControllerError(Exception):
    """Controller failed to respond correctly after retries."""


class controller:
    """Synchronous class to represent a controller."""

    def __init__(self, timeout=1000):
        """Init base object to control lighting controller."""
        # find pinguino
        pinguino = None
        for bus in usb.busses():
            for dev in bus.devices:
                if dev.idVendor == VENDOR and dev.idProduct == PRODUCT:
                    pinguino = dev
        if not pinguino:
            raise ControllerError("No Controller Found!")
        self.dh = pinguino.open()
        self.dh.setConfiguration(CONFIGURATION)
        self.dh.claimInterface(0)
        self.retries = 2
        self.timeout = timeout

    def _read(self, length):
        buf = self.dh.bulkRead(ENDPOINT_IN, length, self.timeout)
        return "".join([chr(i) for i in buf])

    def _write(self, buf):
        return self.dh.bulkWrite(ENDPOINT_OUT, buf, self.timeout)

    def send(self, msg: str):
        for i in range(self.retries):
            self._write(msg)
            ret = self._read(64)
            if "Error" not in ret:
                return ret
        raise ControllerError(ret.decode())

    def set_brightness(self, channel, brightness):
        msg = f"s{channel}{brightness:03d}"
        return self.send(msg)

    def get_brightness(self, channel):
        msg = f"g{channel:01d}"
        return self.send(msg)


class channel:
    """Class to represent a particular channel on the controller."""

    def __init__(self, controller: controller, output):
        self.controller = controller
        self.output = output
        self.value = self._query()

    def _query(self):
        """Get current value from controller."""
        return self.controller.get_value(self.output)

    def _set_value(self):
        """Write value for channel to controller."""
        self.controller.set_value(self.value)

    def set_brightness(self, brightness):
        """Brightness is an integer between 0 and 255."""
        self.value = 255 - brightness
        self._set_value()

    def get_brightness(self):
        """Get brightness for channel as an integer between 0 and 255."""
        return 255 - self.value

    def set_percent_brightness(self, brightness):
        """Set brightness for channel as a percentage, represented by a float between 0
        and 1."""
        self.value = 255 - (brightness * 255)

    def get_percent_brightness(self):
        """Get brightness for channel as a percentage, represented by a float between 0
        and 1."""
        return (255 - self.value) / 255


if __name__ == "__main__":
    cont = controller()
    errors = 0
    success = 0
    for i in range(10):
        for i in range(257):
            try:
                cont.set_brightness(7,i)
                success += 1
            except ControllerError:
                errors += 1

    print(f"With BULK, errors={errors}")


# cont = controller("/dev/ttyACM0", 9600, dsrdtr=False)
# errors = 0
# success = 0
# for i in range(10):
#     for i in range(256):
#         try:
#             cont.set_brightness(i, 7)
#             success += 1
#         except ControllerError:
#             errors += 1

# print(f"Without DTR, errors={errors}")

fade:
- create coroutine and add to background event loop, which does:
for i in range(span):
    set_brightness(channel, i)
    await asyncio.sleep(delay)

Then to cancel fading you just have to cancel the coroutine
either future.cancel() (raises exception which needs catching) or just check for a var to continue
