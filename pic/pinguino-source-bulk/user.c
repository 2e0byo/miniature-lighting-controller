#include <string.h>

u8 * inputPTR;
char buffer[64];
u8 receivedbyte;



void setup()
{
  /* setup ports */
  TRISD = 0x0;
  TRISE &= 0b11111110;			/* E0 as output */
  LED = 1;
  LATD = 0x00;
  /* /\* setup timer 2 *\/ */
  T2CON = 0b11111111;		/* 16 pre- and -post scale */
  PR2 = 1;
  TMR2IP = 1;
  TMR2IF = 0;
  TMR2IE = 1;
  
  
  /* /\* enable interrupt priority *\/ */
  /* IPEN = 1; */
  /* /\* enable interrupts macro *\/ */
  /* ei(); */
}



unsigned char ascii_to_decimal(byte nybble) {
  unsigned char n;
  n = nybble - '0';
  if (n>48) {
    n -= 39;
  }
  return n;
}

void print_error() {
  BULK_printf("Error: invalid input: %s ", buffer);
}

void loop()
{
  while (1) {
    receivedbyte=0;

    while(!BULK_available());	/* block */
    
    receivedbyte = BULK_read(buffer); /* returns index of end */

    buffer[receivedbyte] = 0;	/* set to 0 as c-string */

    if (receivedbyte == 0) continue;

    unsigned char channel;
    unsigned long duty;
    inputPTR = buffer;
  

    if (!strncmp(inputPTR, "s", 1)) {
      if (receivedbyte != 5) {
	/* test_print(inputPTR); */
	print_error();
	continue;
      }

      channel = ascii_to_decimal(inputPTR[1]);
      if (channel >= 8) {
	print_error();
	continue;
      }

      duty = 100* ascii_to_decimal(inputPTR[2]) + 10*ascii_to_decimal(inputPTR[3]) + ascii_to_decimal(inputPTR[4]);
      
      if (duty>256) duty = 256;
      duties[channel] = duty;
      BULK_printf("Set channel %u to %u", channel, duty);
    }
    else if (!strncmp(inputPTR, "g", 1)) {
      if (receivedbyte != 2)
	{
	  /* test_print(inputPTR); */
	  print_error();
	  continue;
	}
      channel = ascii_to_decimal(inputPTR[1]);
      if (channel >= 8) {
	print_error();
	continue;
      }
      
      BULK_printf("Channel %u is %u", channel, duties[channel]);
    }
  }
}
