#define LED LATEbits.LATE0

volatile unsigned long duties[] = {256,256,256,256,256,256,256,256};
volatile unsigned long newDuties[] = {256,256,256,256,256,256,256,256};
volatile unsigned char count = 0;
void channels_interrupt() {
  /* pwm timer */
  ++count;
  /* if (++count == 255) count = 0; */
  unsigned char latch = LATD;
  if (count == 0) {
    latch = 0;
    for (unsigned char i=0; i<8; i++) {
      duties[i] = newDuties[i];
    }
    /* memcpy(duties, newDuties, 8); */
  }
  for (unsigned char i=0; i<8; i++) {
    if (duties[i] == count) latch |= (0x1 << i);
  }
  LATD = latch;
  TMR2IF = 0;
}
